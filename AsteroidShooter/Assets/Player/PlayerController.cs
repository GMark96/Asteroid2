﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    private Rigidbody RigidBody;

	void Start()
    {
        RigidBody = GetComponent<Rigidbody>();
	}
	
	void Update()
    {
        float horizontal = 
            Input.GetAxis("Horizontal");
        float vertical = 
            Input.GetAxis("Vertical");

        Vector3 force =
            new Vector3(horizontal, 0.0f, vertical);

        RigidBody.AddForce(force.normalized);
    }
}
